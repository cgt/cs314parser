package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	tokenizeOnly := false
	if len(os.Args) > 1 && os.Args[1] == "tokens" {
		tokenizeOnly = true
	}

	input, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		panic(err)
	}

	l := NewStringLexer(string(input))
	if tokenizeOnly {
		for tok := l.NextToken(); tok.Type != EOF; tok = l.NextToken() {
			fmt.Printf("[%s: %q] ", tok.Type, tok.Literal)
		}
		fmt.Println()
	} else {
		p := NewParser(l)
		program := p.ParseProgram()
		fmt.Println(program)
	}
}
