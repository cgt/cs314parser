package main

import (
	"unicode"
)

type TokenType int

type Token struct {
	Type    TokenType
	Literal string
}

//go:generate stringer -type TokenType
const (
	ILLEGAL TokenType = iota
	EOF
	PROGRAM
	DOT
	BEGIN
	END
	SEMICOLON
	EQUALS
	VARIABLE
	DIGIT
)

type StringLexer struct {
	input   string
	pos     int
	readPos int
	ch      byte
}

func NewStringLexer(input string) *StringLexer {
	l := &StringLexer{input: input}
	l.readChar()
	return l
}

func (l *StringLexer) readChar() {
	if l.readPos >= len(l.input) {
		l.ch = 0
	} else {
		l.ch = l.input[l.readPos]
	}
	l.pos = l.readPos
	l.readPos++
}

func (l *StringLexer) readWord() string {
	pos := l.pos
	for unicode.IsLetter(rune(l.ch)) {
		l.readChar()
	}
	return l.input[pos:l.pos]
}

func (l *StringLexer) skipWhitespace() {
	for unicode.IsSpace(rune(l.ch)) {
		l.readChar()
	}
}

func (l *StringLexer) NextToken() Token {
	var tok Token

	l.skipWhitespace()

	switch l.ch {
	case 0:
		tok.Literal = ""
		tok.Type = EOF
	case '.':
		tok.Literal = "."
		tok.Type = DOT
	case ';':
		tok.Literal = ";"
		tok.Type = SEMICOLON
	case '=':
		tok.Literal = "="
		tok.Type = EQUALS
	case '0', '1', '2':
		tok.Literal = string(l.ch)
		tok.Type = DIGIT
	default:
		if unicode.IsLetter(rune(l.ch)) {
			tok.Literal = l.readWord()
			switch tok.Literal {
			case "program":
				tok.Type = PROGRAM
			case "begin":
				tok.Type = BEGIN
			case "end":
				tok.Type = END
			case "a", "b", "c":
				tok.Type = VARIABLE
			default:
				tok.Type = ILLEGAL
			}
			return tok
		}

		tok.Literal = string(l.ch)
		tok.Type = ILLEGAL
	}

	l.readChar()
	return tok
}
