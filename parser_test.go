package main

import "testing"

type MockLexer struct {
	tokens []Token
	next   int
}

func (l *MockLexer) NextToken() Token {
	token := l.tokens[l.next]
	l.next++
	return token
}

func TestParseAssign(t *testing.T) {
	l := &MockLexer{tokens: []Token{
		{VARIABLE, "a"},
		{EQUALS, "="},
		{DIGIT, "2"},
		{EOF, ""},
	}}
	p := NewParser(l)

	assign := p.ParseAssign()

	if assign.Variable.Type != VARIABLE {
		t.Errorf("expected variable token type VARIABLE, got %s", assign.Variable.Type)
	}
	if assign.Variable.Literal != "a" {
		t.Errorf("expected variable token literal \"a\", got %q", assign.Variable.Literal)
	}

	if assign.Expr.Type != DIGIT {
		t.Errorf("expected expr token type DIGIT, got %s", assign.Expr.Type)
	}
	if assign.Expr.Type != DIGIT {
		t.Errorf("expected expr token literal \"2\", got %q", assign.Expr.Literal)
	}
}

func TestParseBlock(t *testing.T) {
	l := &MockLexer{tokens: []Token{
		{BEGIN, "begin"},
		{VARIABLE, "a"},
		{EQUALS, "="},
		{DIGIT, "0"},
		{SEMICOLON, ";"},
		{VARIABLE, "c"},
		{EQUALS, "="},
		{DIGIT, "2"},
		{END, "end"},
		{EOF, ""},
	}}
	p := NewParser(l)

	block := p.ParseBlock()

	if n := len(block.Statements); n != 2 {
		t.Fatalf("expected two statements in block, got %d", n)
	}

	s0 := block.Statements[0]
	if typ := s0.NodeType(); typ != new(Assign).NodeType() {
		t.Errorf("statement 0: expected assign, got %s", typ)
	}
	s1 := block.Statements[0]
	if typ := s1.NodeType(); typ != new(Assign).NodeType() {
		t.Errorf("statement 1: expected assign, got %s", typ)
	}
}

func TestParseProgram(t *testing.T) {
	l := &MockLexer{tokens: []Token{
		{PROGRAM, "program"},
		{BEGIN, "begin"},
		{VARIABLE, "a"},
		{EQUALS, "="},
		{DIGIT, "1"},
		{END, "end"},
		{DOT, "."},
		{EOF, ""},
	}}
	p := NewParser(l)

	program := p.ParseProgram()

	if program.Block == nil {
		t.Fatalf("expected non-nil block in program, got nil")
	}
}
