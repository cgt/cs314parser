package main

import "testing"

func TestNextToken(t *testing.T) {
	input := "program . begin end ; = a b c 0 1 2"

	tests := []struct {
		expectedType    TokenType
		expectedLiteral string
	}{
		{PROGRAM, "program"},
		{DOT, "."},
		{BEGIN, "begin"},
		{END, "end"},
		{SEMICOLON, ";"},
		{EQUALS, "="},
		{VARIABLE, "a"},
		{VARIABLE, "b"},
		{VARIABLE, "c"},
		{DIGIT, "0"},
		{DIGIT, "1"},
		{DIGIT, "2"},
	}

	lexer := NewStringLexer(input)
	for i, tt := range tests {
		tok := lexer.NextToken()

		if tok.Type != tt.expectedType {
			t.Errorf("tests[%d] ERR - expected token type %s, got %s", i, tt.expectedType, tok.Type)
		} else {
			t.Logf("tests[%d] OK - expected token type %s, got %s", i, tt.expectedType, tok.Type)
		}
		if tok.Literal != tt.expectedLiteral {
			t.Errorf("tests[%d] ERR - expected literal %q, got %q", i, tt.expectedLiteral, tok.Literal)
		} else {
			t.Logf("tests[%d] OK - expected literal %q, got %q", i, tt.expectedLiteral, tok.Literal)
		}
	}
}
