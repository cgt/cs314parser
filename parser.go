package main

import (
	"bytes"
	"fmt"
)

type Lexer interface {
	NextToken() Token
}

type Parser struct {
	l     Lexer
	token Token
}

func NewParser(l Lexer) *Parser {
	p := &Parser{l: l}
	p.nextToken()
	return p
}

func (p *Parser) nextToken() {
	p.token = p.l.NextToken()
}

type Node interface {
	NodeType() string
	String() string
}

type Program struct {
	Block *Block
}

func (p *Program) NodeType() string { return "program" }
func (p *Program) String() string {
	return fmt.Sprintf("program %v .", p.Block)
}

func (p *Parser) ParseProgram() *Program {
	if p.token.Type == PROGRAM {
		p.nextToken()
		block := p.ParseBlock()
		if p.token.Type == DOT {
			return &Program{block}
		} else {
			panic("program: expected DOT following block")
		}
	} else {
		panic("program: expected 'program'")
	}
}

type Statement interface {
	Node
	IsStatement() bool
}

type Block struct {
	Statements []Statement
}

func (b *Block) IsStatement() bool { return true }
func (b *Block) NodeType() string  { return "block" }
func (b *Block) String() string {
	var buf bytes.Buffer
	buf.WriteString("begin ")
	for _, stmt := range b.Statements {
		buf.WriteString(stmt.String())
		buf.WriteString("; ")
	}
	buf.WriteString("end")
	return buf.String()
}

func (p *Parser) ParseBlock() *Block {
	if p.token.Type != BEGIN {
		panic("block: expected 'begin'")
	}

	block := &Block{}

	p.nextToken()
	for {
		if p.token.Type == VARIABLE {
			assign := p.ParseAssign()
			block.Statements = append(block.Statements, assign)
		} else if p.token.Type == SEMICOLON {
			p.nextToken()
			assign := p.ParseAssign()
			block.Statements = append(block.Statements, assign)
		} else if p.token.Type == END {
			break
		} else {
			panic(fmt.Sprintf("block stmtlist: unexpected %v", p.token))
		}
	}

	p.nextToken()
	return block
}

type Assign struct {
	Variable Token
	Expr     Token
}

func (a *Assign) NodeType() string  { return "assign" }
func (a *Assign) IsStatement() bool { return true }
func (a *Assign) String() string {
	return fmt.Sprintf("%s = %s", a.Variable, a.Expr)
}

func (p *Parser) ParseAssign() *Assign {
	if p.token.Type != VARIABLE {
		panic("assign: expected VARIABLE")
	}

	assign := &Assign{
		Variable: p.token,
	}

	p.nextToken()
	if p.token.Type != EQUALS {
		panic("assign: expected '=' after VARIABLE")
	}

	p.nextToken()
	if p.token.Type != DIGIT {
		panic("assign: expected DIGIT after '='")
	}
	assign.Expr = p.token

	p.nextToken()
	return assign
}
